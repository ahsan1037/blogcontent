USE [SpTest]
GO
/****** Object:  Table [dbo].[Product]    Script Date: 9/22/2014 2:10:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product](
	[ID] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[PurchaseDate] [datetime] NOT NULL,
	[UnitPrice] [decimal](18, 6) NOT NULL,
	[Unit] [int] NOT NULL,
	[IsOrderPaid] [bit] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
