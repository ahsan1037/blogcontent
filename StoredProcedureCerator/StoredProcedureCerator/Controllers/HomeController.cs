﻿using System.Web.Mvc;
using SPCreatorLibrery;
using StoredProcedureCerator.Models;

namespace StoredProcedureCerator.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        private readonly Settings _settings;
        private readonly StoredProcedureGenerator _storedProcedureGenerator;

        public HomeController()
        {
            _settings = new Settings();
            _storedProcedureGenerator = new StoredProcedureGenerator(_settings);
        }

        public ActionResult Index()
        {
            var model = new SettingsModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Index(SettingsModel model)
        {
            _settings.Login = model.Login;
            _settings.Password = model.Password;
            _settings.ServerName = model.ServerName;
            _settings.DatabaseName = model.DatabaseName;

            _settings.SetServer();
            _settings.SetDatabase();

            switch (_storedProcedureGenerator.GenerateAddItemScript())
            {
                case 1:
                    return RedirectToAction("SpCreationSuccessful");
                default:
                    return RedirectToAction("SpCreationFailed");
            }
        }

        public ActionResult SpCreationSuccessful()
        {
            return View();
        }

        public ActionResult SpCreationFailed()
        {
            return View();
        }

    }
}
