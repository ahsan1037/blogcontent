﻿using Microsoft.SqlServer.Management.Smo;

namespace SPCreatorLibrery
{
    public class Settings
    {
        public string ServerName { get; set; }
        public string DatabaseName { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public Server Server { get; set; }
        public Database Database { get; set; }

        public void SetServer()
        {
            Server = new Server(ServerName);
            Server.ConnectionContext.LoginSecure = false;
            Server.ConnectionContext.Login = Login;
            Server.ConnectionContext.Password = Password;
        }
        public void SetDatabase()
        {
            Database = Server.Databases[DatabaseName];
        }

        public void Connect()
        {
            SetServer();
            Server.ConnectionContext.Connect();
            SetDatabase();
        }
        public void Disconnect()
        {
            if (Server.ConnectionContext.IsOpen)
                Server.ConnectionContext.Disconnect();
        }
    }
}
