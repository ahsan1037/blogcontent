﻿using System;
using Microsoft.SqlServer.Management.Smo;

namespace SPCreatorLibrery
{
    public class StoredProcedureGenerator
    {
        private readonly Settings _settings;

        public StoredProcedureGenerator(Settings settings)
        {
            _settings = settings;
        }

        public int GenerateAddItemScript()
        {

            var storedProcedure =
                new StoredProcedure(_settings.Database, "Product_AddProduct", "dbo");

            if (_settings.Database.StoredProcedures.Contains(storedProcedure.Name))
            {
                storedProcedure.Refresh();
                storedProcedure.Drop();
            }

            storedProcedure =
                new StoredProcedure(_settings.Database, "Product_AddProduct", "dbo");
            storedProcedure.TextMode = false;
            storedProcedure.AnsiNullsStatus = false;
            storedProcedure.QuotedIdentifierStatus = false;

            StoredProcedureParameter param;

            param = new StoredProcedureParameter(storedProcedure, "@ID", DataType.UniqueIdentifier);
            storedProcedure.Parameters.Add(param);
            param = new StoredProcedureParameter(storedProcedure, "@Name", DataType.NVarChar(500));
            storedProcedure.Parameters.Add(param);
            param = new StoredProcedureParameter(storedProcedure, "@PurchaseDate", DataType.DateTime);
            storedProcedure.Parameters.Add(param);
            param = new StoredProcedureParameter(storedProcedure, "@UnitPrice", DataType.Decimal(6,18));
            storedProcedure.Parameters.Add(param);
            param = new StoredProcedureParameter(storedProcedure, "@Unit", DataType.Int);
            storedProcedure.Parameters.Add(param);
            param = new StoredProcedureParameter(storedProcedure, "@IsOrderPaid", DataType.Bit);
            storedProcedure.Parameters.Add(param);


            var script = "BEGIN\nDeclare @IDCount [int];\n\nSELECT @IDCount = COUNT([ID]) FROM [dbo].[Product]\nWHERE [ID] = @ID\n\n";

            script += "IF @IDCount = 0\n\tBEGIN\n\t\tINSERT INTO [dbo].[Product]\n\t\t\t(\n\t\t\t\t";
            script += "[ID]\n\t\t\t\t";
            script += ",[Name]\n\t\t\t\t";
            script += ",[PurchaseDate]\n\t\t\t\t";
            script += ",[UnitPrice]\n\t\t\t\t";
            script += ",[Unit]\n\t\t\t\t";
            script += ",[IsOrderPaid]\n\t\t\t\t";
            script += ")\n\t\t\tVALUES(";


            script += "@ID\n";
            script += "\t\t\t\t,@Name\n";
            script += "\t\t\t\t,@PurchaseDate\n";
            script += "\t\t\t\t,@UnitPrice\n";
            script += "\t\t\t\t,@Unit\n";
            script += "\t\t\t\t,@IsOrderPaid\n";
            script += "\t\t\t)\n\tEND\nEND";
            storedProcedure.TextBody = script;

            try
            {
                storedProcedure.Create();
                return 1;
            }
            catch (Exception e)
            {
                throw new Exception("StoredProcedure Creation Error", e);
            }
        }
    }
}
