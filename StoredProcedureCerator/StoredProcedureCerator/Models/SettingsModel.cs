﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StoredProcedureCerator.Models
{
    public class SettingsModel
    {
        public string ServerName { get; set; }
        public string DatabaseName { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
    }
}